<?php

class Html
{
    private $title;
    private $icon;
    private $html;
    private $head;
    private $body;
    private $style;
    private $script;
    private $meta;
    private $content;

    public function __construct()
    {
        $this->title = [];
        $this->icon = [];
        $this->html = [];
        $this->head = [];
        $this->body = [];
        $this->style = [];
        $this->script = [];
        $this->meta = [];
        $this->content = '';

        // Cargando meta
        $this->loadMeta(['http-equiv' => 'Content-Type', 'content' => 'text/html;charset=UTF-8']);
        $this->loadMeta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
    }

    public function newElement(){
        return new class{
            private $element = [];
            private $elementHappend = [];
        
            public function createElement(string $name)
            {
                $this->element['name'] = $name;
                $this->element['end'] = in_array(strtolower($name), ['input', 'br']);
            }
        
            public function setAttribute(array $attr)
            {
                $this->element['attr'] = $attr;
            }
        
            public function setText(string $text)
            {
                $this->element['text'] = $text;
            }
        
            public function happend(object $element)
            {
                if (!isset($this->element['happend'])) {
                    $this->element['happend'] = [];
                }
                array_push($this->element['happend'], $element->getElement());
                array_push($this->elementHappend, $element);
            }
        
            public function getElement()
            {
                return $this->element;
            }
        
            private function getAttr()
            {
                $attr = '';
                if (isset($this->element['attr'])) {
                    foreach ($this->element['attr'] as $index => $value) {
                        $attr .= " $index='$value'";
                    }
                    $attr = str_replace('"', '\"', $attr);
                }
                return $attr;
            }
        
            public function getHTML()
            {
                $acum = '';
                if (isset($this->element['name'])) {
                    $acum = "<{$this->element['name']}";
                    $acum .= $this->getAttr() . '>';
        
                    if(isset($this->element['text'])){
                        $acum .= $this->element['text'];
                    }
        
                    if (isset($this->element['happend'])) {
                        foreach($this->elementHappend as $element){
                            $acum .= $element->getHTML();
                        }
                    }
        
                    if (!$this->element['end']) {
                        $acum .= "</{$this->element['name']}>";
                    }
                }
                return $acum;
            }
        };
    }

    public function setTitle(string $title)
    {
        $this->title = [
            'start' => '<title>',
            'text' => $title,
            'end' => '</title>'
        ];
    }

    public function setIconPage(string $url)
    {
        $this->icon = [
            'start' => '<link',
            0 => ' ',
            1 => "rel='",
            'rel' => 'icon',
            2 => "' ",
            3 => "href='",
            'href' => $url,
            4 => "' ",
            5 => "type='",
            'type' => 'image/png',
            'end' => "'>"
        ];
    }

    public function loadMeta(array $meta)
    {
        $acum = '<meta ';
        foreach ($meta as $index => $value) {
            $acum .= "$index='$value' ";
        }
        $acum = trim($acum) . '>';
        array_push($this->meta, $acum);
    }

    public function loadScripts(array $urls){
        foreach($urls as $url){
            array_push($this->script, "<script src='$url'></script>");
        }
    }
    
    public function loadStyles(array $urls){
        foreach($urls as $url){
            array_push($this->style, "<link rel='stylesheet' href='$url' type='text/css'>");
        }
    }

    public function loadHTML(string $html){
        $this->content .= str_ireplace('script', 'i style="color: red;"', $html);
    }

    public function innerHTML(string $html){
        $this->content = str_ireplace('script', 'i style="color: red;"', $html);
    }

    public function loadFileHTML(string $url, array $data = []){
        ob_start();
        include $url;
        $html = ob_get_clean();
        $this->content .= str_ireplace('script', 'i style="color: red;"', $html);
    }

    public function ln(int $cant = 1){
        for($i=0; $i<$cant; $i++){
            $this->content .= '<br>';
        }
    }

    public function output()
    {
        $this->head = [
            'meta' => implode($this->meta),
            'title' => implode($this->title),
            'icon' => implode($this->icon),
            'style' => implode($this->style)
        ];

        $this->body = [
            'content' => $this->content,
            'script' => implode($this->script)
        ];

        $this->html = [
            'head' => implode($this->head),
            'body' => implode($this->body)
        ];

        echo implode($this->html);
    }

    public function getTitle()
    {
        return isset($this->title['text']) ? $this->title['text'] : '';
    }

    public function getIconPage(string $atribute='href')
    {
        return isset($this->icon[$atribute]) ? $this->icon[$atribute] : '';
    }
}