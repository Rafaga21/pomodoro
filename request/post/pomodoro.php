<?php

include '../../controller/pomodoro.php';

$pomodoro = new Pomodoro('../../');

if(!empty($_POST)){
    if(isset($_POST['action'])){
        if($_POST['action'] == 'update'){
            echo $pomodoro->updateTime($_POST['id']);
        }elseif($_POST['action'] == 'insert'){
            if(!empty($_POST['title'])){
                echo $pomodoro->newPomodoro([
                    'title' => $_POST['title'],
                    'time' => 1500,
                    'status' => 'line',
                    'delete' => false
                ]);
            }else{
                echo '{"message": "Debe Agregar un Título al Pomodoro."}';
            }
        }elseif($_POST['action'] == 'reset'){
            echo $pomodoro->resetPomodoro($_POST['id']);
        }elseif($_POST['action'] == 'delete'){
            $pomodoro->deletePomodoro($_POST['id']);
        }
    }else{
        echo '{"message": "Not Found."}';
    }
}