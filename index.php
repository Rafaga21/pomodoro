<?php

include './etc/Html.php';
include './etc/SearchFilePath.php';
include './controller/pomodoro.php';

$page = SearchFilePath::getFiles();
$views = $page['VIEW'];
$view = isset($_GET['v'])?$_GET['v']:'index';
$view = isset($views['FILE_DIR'][$view])?$view:'404';

$pomodoro = new Pomodoro();
$html = new Html();

$html->setIconPage($page['ASSETS']['IMAGE']['FILE_DIR']['pomodoro']);
$html->setTitle('Pomodoro');

$html->loadScripts([
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js',
    $page['ASSETS']['JS']['FILE_DIR']['script']
]);

$html->loadStyles([
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
    $page['ASSETS']['CSS']['FILE_DIR']['style']
]);

$html->loadFileHTML($views['FILE_DIR'][$view], [
    'get' => $_GET,
    'post' => $_POST,
    'view' => $view,
    'assets' => $page['ASSETS'],
    'pomodoro' => $pomodoro->getPomodoros()
]);

$html->output();