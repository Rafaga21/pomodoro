<div class="container text-center text-white" id="e404">
    <h1>Ups...</h1>
    <h2>No Tienes Acceso a Estos Archivos.</h2>
    <h4 class="text-muted">Error 403</h4>
    <a href="./" class="btn btn-info text-white">Volver</a>
</div>