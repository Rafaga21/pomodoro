<div class="container mt-5">
    <h1 class="text-muted text-center">T&eacute;cnica de Pomodoro</h1>
    <div class="mt-4"></div>
    
    <button class="btn btn-info text-white" data-bs-toggle="modal" data-bs-target="#m-pomodoro">Nuevo Pomodoro</button>
    <!-- Modal -->
    <div class="modal fade" id="m-pomodoro" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Pomodoro</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="" class="form-label">T&iacute;tulo de Pomodoro</label>
                    <input type="text" id="txtTitle" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="sendData()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dropdown-divider bg-white mt-4"></div>
    <div class="row bg-dark text-white">
        <div class="col bg-gray">En Cola</div>
        <div class="col">En Proceso</div>
        <div class="col">Finalizados</div>
    </div>
    <div class="dropdown-divider bg-white"></div>
    <div class="row bg-dark">
        <div class="col" id="line"></div>
        <div class="col" id="proccessing"></div>
        <div class="col" id="ending"></div>
    </div>
    <div class="mt-4"></div>
    <audio id="sound-proccessing">
        <source src="<?=$data['assets']['SOUND']['FILE_DIR']['proccess']?>">
    </audio>
    <audio id="sound-ending">
        <source src="<?=$data['assets']['SOUND']['FILE_DIR']['ending']?>">
    </audio>
    <audio id="sound-in-proccess">
        <source src="<?=$data['assets']['SOUND']['FILE_DIR']['inproccess']?>">
    </audio>
</div>