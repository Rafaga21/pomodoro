<?php

session_start();

class Pomodoro
{
    private $id;
    private $path;

    /**
     * Inicializa las variables y crea un archivo segun el id generado
     * 
     * @param string $position recive la ubicación del archivo.
     * @return void sin retorno.
     * @author Rafael Minaya Beltrán.
     * @access public
     * @version 1.0
     */
    function __construct(string $position='./')
    {
        if (!isset($_SESSION['id'])) {
            $_SESSION['id'] = md5($_SERVER['REMOTE_ADDR'] . rand(1, 1000));
        }
        $this->id = $_SESSION['id'];
        $this->path = "{$position}assets/data/$this->id.json";

        if(!file_exists($this->path)){
            file_put_contents($this->path, '[]');
        }
    }

    /**
     * Crea un nuevo pomodoro
     * 
     * @access public
     * @param array $pomodoro recive un arreglo con los datos a guardar.
     * @return string retorna un json convertido en string.
     * @version 1.0
     * @author Rafael Minaya Beltrán.
     */
    public function newPomodoro(array $pomodoro)
    {
        $pomodoros = json_decode(file_get_contents($this->path), true);
        $pomodoro['id'] = 'p-' . md5(count($pomodoros));
        array_push($pomodoros, $pomodoro);
        file_put_contents($this->path, json_encode($pomodoros));
        return json_encode($pomodoro);
    }

    /**
     * Retorna todos los pomodoros que no han sido eliminados.
     * 
     * @access public
     * @return array retorna un arreglo de pomodoros.
     * @version 1.0
     * @author Rafael Minaya Beltrán.
     */
    public function getPomodoros(){
        $data = json_decode(file_get_contents($this->path), true);
        $pomodoros = [];

        foreach($data as $pomodoro){
            if(!$pomodoro['delete']){
                array_push($pomodoros, $pomodoro);
            }
        }
        return $pomodoros;
    }

    /**
     * Actualiza el tiempo de un pomodoro en especifico.
     * 
     * @access public
     * @param string $idPomodoro recive el id del pomodoro que será actualizado.
     * @return string retorna un json convertido en string.
     * @version 1.0
     * @author Rafael Minaya Beltrán.
     */
    public function updateTime(string $idPomodoro){
        $pom = [];
        $pomodoros = json_decode(file_get_contents($this->path), true);
        foreach($pomodoros as $index => $pomodoro){
            if($pomodoro['id'] == $idPomodoro){
                $pomodoros[$index]['time'] = --$pomodoro['time'];
                if($pomodoro['time'] == 0){
                    $pomodoros[$index]['status'] = "ending";
                    $pomodoros[$index]['time'] = 1500;
                    $pomodoro['time'] = $pomodoros[$index]['time'];
                    $pomodoro['status'] = "ending";
                }elseif($pomodoros[$index]['status'] == 'line'){
                    $pomodoros[$index]['status'] = "proccessing";
                    $pomodoro['status'] = "proccessing";
                }
                break;
            }
        }
        file_put_contents($this->path, json_encode($pomodoros));
        return json_encode($pomodoro);
    }

    /**
     * Reinicia un pomodoro cuando este ha finalizado.
     * 
     * @access public
     * @param string $id recive el id del pomodoro que será reiniciado.
     * @return string retorna un json convertido en string del pomodoro reiniciado.
     * @version 1.0
     * @author Rafael Minaya Beltrán.
     */
    public function resetPomodoro(string $id){
        $pomodoros = json_decode(file_get_contents($this->path), true);
        foreach($pomodoros as $index => $pomodoro){
            if($pomodoro['id'] == $id){
                $pomodoros[$index]['status'] = 'line';
                break;
            }
        }
        file_put_contents($this->path, json_encode($pomodoros));
        return json_encode($pomodoro);
    }

    /**
     * Elimina un pomodoro mediante el id
     * 
     * @access public
     * @param string $id recive el id de un pomodoro.
     * @return void
     * @version 1.0
     * @author Rafael Minaya Beltrán.
     */
    public function deletePomodoro(string $id){
        $pomodoros = json_decode(file_get_contents($this->path), true);
        foreach($pomodoros as $index => $pomodoro){
            if($pomodoro['id'] == $id){
                $pomodoros[$index]['delete'] = true;
                break;
            }
        }
        file_put_contents($this->path, json_encode($pomodoros));
    }
}
