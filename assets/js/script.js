
let idInterval = 0;

function sendData() {
    var title = $('#txtTitle').val();
    $('#txtTitle').val('');
    $.post(
        './request/post/pomodoro.php',
        {
            action: 'insert',
            title: title
        }, (data) => {
            data = JSON.parse(data);
            addPomodoro(data);
        }
    );
}

function cleaning() {
    $('#line')[0].innerHTML = '';
    $('#proccessing')[0].innerHTML = '';
    $('#ending')[0].innerHTML = '';
}

function getMinut(second) {
    let m = 0;
    while (second >= 60) {
        m++;
        second -= 60;
    }
    return m + ' m ' + second + ' s';
}

function exists(parent, child) {
    for (var index in parent) {
        if (parent[index] == child) {
            return true;
        }
    }
    return false;
}

function onClickBtn(value, card, span, ...button) {
    $('#sound-proccessing')[0].play();
    //document.getElementById(value.status).removeChild(card);
    document.getElementById('proccessing').appendChild(card);

    card.childNodes[2].replaceChild(button[1], button[0]);
    //button[0].disabled = true;
    idInterval = setInterval(() => {
        $.post(
            './request/post/pomodoro.php',
            {
                action: 'update',
                id: value.id
            }, (val) => {
                val = JSON.parse(val);
                document.getElementById(span.id).innerHTML = getMinut(val.time);
                if (val.status == 'ending') {
                    $('#sound-ending')[0].play();
                    card.childNodes[2].replaceChild(button[2], button[1]);
                    document.getElementById(val.status).appendChild(card);
                    clearInterval(idInterval);
                    idInterval = 0;
                }
            }
        );
    }, 1000);
}

function addPomodoro(value){
    var card = document.createElement('div');
            var cHeader = document.createElement('div');
            var cBody = document.createElement('div');
            var cFooter = document.createElement('div');

            var btnstart = document.createElement('button');
            var btnstop = document.createElement('button');
            var btnreset = document.createElement('button');
            var btndelete = document.createElement('button');

            var span = document.createElement('span');
            var txtBody = document.createTextNode(value.title);
            var txtTime = document.createTextNode(getMinut(value.time));

            card.className = 'card mt-4';
            card.id = value.id;
            card.style.width = '15rem';

            cHeader.className = 'card-header';

            cBody.className = 'card-body text-dark';

            cFooter.className = 'card-footer text-end';

            btnstart.className = 'btn btn-info text-white';
            btnstart.innerHTML = '<i class="fas fa-play"></i>';
            btnstart.onclick = () => {
                if(idInterval == 0){
                    onClickBtn(value, card, span, btnstart, btnstop, btnreset, btndelete);
                }else{
                    document.getElementById('sound-in-proccess').play();
                }
            }

            btnstop.className = 'btn btn-danger text-white';
            btnstop.innerHTML = '<i class="fas fa-stop"></i>';
            btnstop.onclick = () => {
                clearInterval(idInterval);
                idInterval = 0;
                card.childNodes[2].replaceChild(btnstart, btnstop);
            }

            btndelete.className = 'btn btn-danger text-white';
            btndelete.innerHTML = '<i class="fas fa-trash-alt"></i>';
            btndelete.style.margin = '5px';
            btndelete.setAttribute('data-bs-toggle', 'tooltip');
            btndelete.setAttribute('data-bs-placement', 'top');
            btndelete.title = 'Borrar';
            btndelete.onclick = () => {
                if (idInterval != 0) {
                    clearInterval(idInterval);
                }

                $.post(
                    './request/post/pomodoro.php',
                    {
                        action: 'delete',
                        id: value.id
                    }, (dat) => {
                        if (exists(document.getElementById('line'), card)) {
                            document.getElementById('line').removeChild(card);
                        } else if (exists(document.getElementById('proccessing'), card)) {
                            document.getElementById('proccessing').removeChild(card);
                        } else if (exists(document.getElementById('ending'), card)) {
                            document.getElementById('ending').removeChild(card);
                        }
                    }
                );
            }

            btnreset.className = 'btn btn-info text-white';
            btnreset.innerHTML = '<i class="fas fa-retweet"></i>';
            btnreset.onclick = () => {
                $.post(
                    './request/post/pomodoro.php',
                    {
                        action: "reset",
                        id: value.id
                    }, (datapomodoro) => {
                        document.getElementById('ending').removeChild(card);
                        document.getElementById('line').appendChild(card);
                        card.childNodes[2].replaceChild(btnstart, btnreset);
                    }
                );
            }

            span.id = 'time_' + value.id;
            span.appendChild(txtTime);
            span.className = 'text-dark';

            cHeader.appendChild(span);
            cBody.appendChild(txtBody);

            cFooter.appendChild(btndelete);

            if (value.status == 'proccessing') {
                cFooter.appendChild(btnstart);
            } else if (value.status == 'ending') {
                cFooter.appendChild(btnreset);
            } else {
                cFooter.appendChild(btnstart);
            }

            card.appendChild(cHeader);
            card.appendChild(cBody);
            card.appendChild(cFooter);

            document.getElementById(value.status).appendChild(card);
}

$.get(
    './request/get/pomodoro.php',
    (data) => {
        data = JSON.parse(data);

        data.forEach((value) => {
            addPomodoro(value);
        });
    }
)